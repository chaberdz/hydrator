<?php

namespace Chaberdz\Hydrator\Hydrator;

use Chaberdz\Chadmin\Controller\PageController;
use Chaberdz\Hydrator\Checker\NameConventionChecker;
use Chaberdz\Hydrator\Converter\CamelCaseToSnakeCaseConverter;
use Chaberdz\Hydrator\Converter\SnakeCaseToCamelCaseConverter;
use Chaberdz\Hydrator\Converter\SnakeCaseToPascalCaseConverter;
use Chaberdz\Hydrator\Enum\NamingConvention;
use Chaberdz\Hydrator\Tests\Example\UserDTO;

class Hydrator
{
    public function hydrate(
        array $arrayToTransform,
        string $targetObjectClass,
        string $converterClass,
        ?array $fields = null
    ) {
        $this->checkIfObjectExist($targetObjectClass);

        $methodListToCheck = isset($fields) ? $fields : \array_keys($arrayToTransform);

        $object = new $targetObjectClass();
        foreach ($methodListToCheck as $propertyName) {
            $methodName = $converterClass::setMethodName($propertyName);

            if (\method_exists($object, $methodName)) {
                $object->$methodName($arrayToTransform[$propertyName]);
                continue;
            }

            if (\property_exists($object, $converterClass::property($propertyName))) {
                $reflectionClass = new \ReflectionClass($targetObjectClass);

                $reflectionProperty = $reflectionClass->getProperty($converterClass::property($propertyName));
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->setValue($object, $arrayToTransform[$propertyName]);
            }
        }

        return $object;
    }

    public function extract($object, string $converterClass, array $fields): array
    {
        $arrayToReturn = [];
        foreach ($fields as $field) {
            $get = SnakeCaseToCamelCaseConverter::getMethodName($field);
            $valueFromObject = $object->$get();

            $arrayToReturn[$field] = $valueFromObject;
        }

        return $arrayToReturn;
    }

    public function extractAll(array $arrayOfObjects, string $converterClass, array $fields): array
    {
        $arrayToReturn = [];

        foreach ($arrayOfObjects as $object) {
            $arrayToReturn[] = $this->extract($object, $converterClass, $fields);
        }

        return $arrayToReturn;
    }

    public function hydrateSnakeCaseToCamelCase(
        array $arrayToTransform,
        $targetObjectClass,
        ?array $whichData = null
    ) {
        return $this->hydrate($arrayToTransform, $targetObjectClass, SnakeCaseToCamelCaseConverter::class, $whichData);
    }

    public function hydrateAll(
        array $twoDimensionalArray,
        string $objectClass,
        string $converterClass,
        ?array $whichData = null
    ): array {
        $arrayToResponse = [];

        foreach ($twoDimensionalArray as $smallArray) {
            $arrayToResponse[] = $this->hydrate(
                $smallArray,
                $objectClass,
                $converterClass,
                $whichData
            );
        }

        return $arrayToResponse;
    }

    private function checkIfObjectExist(string $objectTarget): bool
    {
        $object = new $objectTarget();
        if (false === \is_object($object)) {
            return false;
        }

        return true;
    }
}
