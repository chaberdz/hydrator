<?php

namespace Chaberdz\Hydrator\Converter;

class SnakeCaseToCamelCaseConverter implements IConverter
{
    public static function setMethodName(string $textInSnakeCase): string
    {
        return 'set'.\ucfirst(self::property($textInSnakeCase));
    }

    public static function getMethodName(string $textInSnakeCase): string
    {
        return 'get'.\ucfirst(self::property($textInSnakeCase));
    }

    public static function property(string $textInSnakeCase): string
    {
        return \lcfirst(str_replace('_', '', ucwords($textInSnakeCase, '_')));
    }
}
