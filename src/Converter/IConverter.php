<?php

namespace Chaberdz\Hydrator\Converter;

interface IConverter
{
    public static function setMethodName(string $text): string;
    public static function getMethodName(string $text): string;
    public static function property(string $text): string;
}