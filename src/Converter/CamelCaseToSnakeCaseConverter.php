<?php

namespace Chaberdz\Hydrator\Converter;

class CamelCaseToSnakeCaseConverter implements IConverter
{
    public static function setMethodName(string $textInCamelCase): string
    {
        return 'set_'.self::property($textInCamelCase);
    }

    public static function getMethodName(string $textInCamelCase): string
    {
        return 'get_'.self::property($textInCamelCase);
    }

    public static function property(string $input): string
    {
        $pattern = '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!';
        preg_match_all($pattern, $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ?
                strtolower($match) :
                lcfirst($match);
        }

        return implode('_', $ret);
    }
}
