Biblioteka służy do hydracji danych z postaci tablicy do obiektu oraz w drugą stronę. 
Umożliwia konwersje danych które są zaprezentowane w różnych konwencjach nazewniczych.
Można hydrować jedną tablice lub tablice tablic. Operacja odwrotna czyli ekstrakcja, również może być przeprowadzona na 1 obiekcie lub 
tablicy takich obiektów. Podczas hydracji, dane do obiektu są wstrzykiwane za pomocą metod dostępowych. 
W przypadku braku wymaganej metody dostępowej,  ustawiana jest własność klasy bezpośrednio.
Prosta w obsłudze i lekka biblioteka która pozwoli uniknąć replikacji podobnego kodu.

## Instalacja

1.Dodajemy do pliku composer.json

```
  "require": {
    (...)
    "chaberdz/hydrator": "dev-master"
   },
     "repositories": [
    {
      "type": "vcs",
      "url": "https://chaberdz@bitbucket.org/chaberdz/hydrator.git"
    }
  ],
```

2. Instalujemy paczkę

`composer update chaberdz/hydrator`

2. Dodajemy konfiguracje serwisów

```yaml
#config/services.yaml

imports:
  - { resource: '../vendor/chaberdz/hydrator/config/services.yaml' }
```

3. Gotowe. Z Hydratora można skorzystać przez autowiring, wstrzykując go w poniższy sposób:

```php
use Chaberdz\Hydrator\Hydrator\Hydrator;

(...)

private Hydrator $hydrator;

public function __construct(Hydrator $hydrator)
{
        $this->hydrator = $hydrator;
}
```

## Przyłady użycia

Ponizsze przykłady użycia hydratora będą opierały się o poniższą przykładową klasę.

```php
class UserDTO
{
    private int $id;
    private string $name;
    private string $email;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}

```

### 1. Hydracja wszystkich danych

```php
$arrayToHydrate  = ['id' => 1, 'name' => 'John', 'email' => 'example@example.com'];

$object = $transformer->hydrate(
            $arrayToHydrate,
            UserDTO::class,
            SnakeCaseToPascalCaseConverter::class,
        );
dump($object);
```

Rezultat:

```php
  UserDTO {
    -id: 1
    -name: "John"
    -email: "example@example.com"
```

### 2.Hydracja wybranych danych

Trzeci parametr to klasa konwertera który ma zostać zastosowany do konwersji danych. W ponizszym przykładzie, tablica ma
zapis w konwencji `snake_case` a obiekt w konwerncji `camelCase` więc został użyty
konwerter `SnakeCaseToPascalCaseConverter`. Lista konwerterów znajduje się w dalszej części tego dokumentu. W czwartym
parametrze fukcji `hydrate` można okreslić klucze tablicy które mają być hydrowane.

```php
$arrayToHydrate  = ['id' => 1, 'name' => 'John', 'email' => 'example@example.com'];

$object = $transformer->hydrate(
            $arrayToHydrate,
            UserDTO::class,
            SnakeCaseToPascalCaseConverter::class,
            ['id', 'name']
        );
dump($object);
```

Rezultat:

```php
  UserDTO {
    -id: 1
    -name: "John"
  }
```

### 3. Hydracja tablicy tablic

Hydrator ma możliwość hydracji tablicy tablic.

```php
  $arrayToHydrate  = [
                ['id' => 1, 'name' => 'John', 'email' => 'john@example.com'],
                ['id' => 2, 'name' => 'Bob', 'email' => 'bob@example.com'],
                ];
                
   $userDTOList = $this->transformer->hydrateAll(
            $arrayToHydrate,
            UserDTO::class,
            SnakeCaseToPascalCaseConverter::class
        );
   dump($userDTOList);
```

Rezultat:

```php
  array:2 [▼
  0 =>   UserDTO {
    -id: 1
    -name: "John"
  }
  1 =>   UserDTO {
    -id: 1
    -name: "John"
  }
]
```

### 4. Ekstrakcja obiektów

Hydrator pełni też funkcje ekstraktora, czyli umożliwia konwersje obiektu na tablice. Podczas ekstrakcji danych trzeba
określić pola które mają być przekonwertowane

```php

    $userDTO = new UserDTO();
    $userDTO->setId(3);
    $userDTO->setName('Jack');
    

    $extracted = $this->hydrator->extract($userDTO, CamelCaseToSnakeCaseConverter::class, ['id', 'name']);
    dump($extracted);
   
```

Rezultat:

```php
array:1 [▼
  'id' => 3,
  'name' => 'Jack'
]

### 5.Ekstraktor tablicy obiektów

```

### 5. Ekstrakcja tablicy obiektów

Jest to odwrotna operacja w stosunku do tej opisane w pkt.3 czyli konwersja wielu obiektów do tablicy tablic. Podczas ekstrakcji danych trzeba
określić pola które mają być przekonwertowane.
## Testy jednostkowe

Testy zostały napisane w PHPUnit. Można je uruchomić komendą:
```
php phpunit.phar  vendor/chaberdz/hydrator/tests
```
