<?php

namespace Chaberdz\Hydrator\Tests\src\Converter;

use Chaberdz\Hydrator\Converter\CamelCaseToSnakeCaseConverter;
use PHPUnit\Framework\TestCase;

class CamelCaseToSnakeCaseConverterTest extends TestCase
{
    public function testGetMethod(): void
    {
        $property = 'firstTextToConvert';
        $this->assertEquals('get_first_text_to_convert', CamelCaseToSnakeCaseConverter::getMethodName($property));
    }

    public function testSetMethod(): void
    {
        $property = 'firstTextToConvert';
        $this->assertEquals('set_first_text_to_convert', CamelCaseToSnakeCaseConverter::setMethodName($property));
    }

    public function testProperty(): void
    {
        $property = 'firstTextToConvert';
        $this->assertEquals('first_text_to_convert', CamelCaseToSnakeCaseConverter::property($property));
    }
}
