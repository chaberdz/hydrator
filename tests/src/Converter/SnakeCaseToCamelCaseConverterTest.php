<?php

namespace Chaberdz\Hydrator\Tests\src\Converter;

use Chaberdz\Hydrator\Converter\SnakeCaseToCamelCaseConverter;
use PHPUnit\Framework\TestCase;

class SnakeCaseToCamelCaseConverterTest extends TestCase
{
    public function testGetMethod(): void
    {
        $property = 'first_text_to_convert';
        $this->assertEquals('getFirstTextToConvert', SnakeCaseToCamelCaseConverter::getMethodName($property));
    }

    public function testSetMethod(): void
    {
        $property = 'first_text_to_convert';
        $this->assertEquals('setFirstTextToConvert', SnakeCaseToCamelCaseConverter::setMethodName($property));
    }

    public function testProperty(): void
    {
        $property = 'first_text_to_convert';
        $this->assertEquals('firstTextToConvert', SnakeCaseToCamelCaseConverter::property($property));
    }
}
