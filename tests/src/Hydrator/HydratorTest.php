<?php

namespace Chaberdz\Hydrator\Tests\src\Hydrator;

use Chaberdz\Hydrator\Converter\CamelCaseToSnakeCaseConverter;
use Chaberdz\Hydrator\Converter\SnakeCaseToCamelCaseConverter;
use Chaberdz\Hydrator\Hydrator\Hydrator;
use Chaberdz\Hydrator\Tests\Example\UserDTO;
use PHPUnit\Framework\TestCase;

class HydratorTest extends TestCase
{
    public function testHydrate(): void
    {
        $arrayToHydrate = ['id' => 1, 'username' => 'Joe', 'password_hash' => '123'];

        $hydrator = new Hydrator();

        $object = $hydrator->hydrate(
            $arrayToHydrate,
            UserDTO::class,
            SnakeCaseToCamelCaseConverter::class,
            ['id', 'username', 'password_hash']
        );

        $this->assertInstanceOf(UserDTO::class, $object);
        $this->assertEquals(1, $object->getId());
        $this->assertEquals('Joe', $object->getUsername());
        $this->assertEquals('123', $object->getPasswordHash());
    }

    public function testExtract(): void
    {
        $userDTOMock = $this->getMockBuilder(UserDTO::class)->getMock();

        $userDTOMock->method('getId')->willReturn(4);
        $userDTOMock->method('getUsername')->willReturn('Bob');
        $userDTOMock->method('getPasswordHash')->willReturn('4321');

        $hydrator = new Hydrator();

        $array = $hydrator->extract(
            $userDTOMock,
            CamelCaseToSnakeCaseConverter::class,
            ['id', 'username', 'password_hash']
        );

        $this->arrayHasKey('id', $array);
        $this->arrayHasKey('username', $array);
        $this->arrayHasKey('password_hash', $array);

        $this->assertEquals(4, $array['id']);
        $this->assertEquals('Bob', $array['username']);
        $this->assertEquals('4321', $array['password_hash']);
    }
}
